using Microsoft.Maui.Controls;
using Mopups.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FreelanceMobile.Pages
{
    public partial class HomePage : ContentPage
    {
        private List<View> sections = new List<View>();

        public HomePage()
        {
            InitializeComponent();

            InitializeSections();

            scrollView.Scrolled += OnScroll;
        }

        private void InitializeSections()
        {
            sections.Add(HomepageSection);
            sections.Add(SiapaKamiSection);
            sections.Add(LihatJobSection);
            sections.Add(SkillSection);
        }

        private void OnScroll(object sender, ScrolledEventArgs e)
        {
            double scrollPosition = scrollView.ScrollY;

            // Calculate the index of the active section based on custom conditions
            int activeSectionIndex = CalculateActiveSectionIndex(scrollPosition, scrollView.Height);
            activeSectionIndex = Math.Clamp(activeSectionIndex, 0, sections.Count - 1);

            // Set the corresponding section as the active one
            SetActiveSection(activeSectionIndex);
        }

        private int CalculateActiveSectionIndex(double scrollPosition, double scrollViewHeight)
        {
            // Define your custom conditions to determine the active section
            if (scrollPosition < scrollViewHeight)
            {
                return 0; // First section is active
            }
            else if (scrollPosition < 2 * scrollViewHeight)
            {
                return 1; // Second section is active
            }
            else if (scrollPosition < 3 * scrollViewHeight)
            {
                return 2; // Third section is active
            }
            else
            {
                return 3; // Fourth section is active
            }
        }


        private void SetActiveSection(int activeIndex)
        {
            foreach (var section in sections)
            {
                section.IsVisible = false;
            }

            sections[activeIndex].IsVisible = true;
        }

        private void LoginButton_Clicked(object sender, EventArgs e)
        {
            MopupService.Instance.PushAsync(new FormLogin());
        }

        private async void RegisterButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RegisterPage());
        }

        private async void SiapaKamiButton_Clicked(object sender, EventArgs e)
        {
            await scrollView.ScrollToAsync(SiapaKamiSection, ScrollToPosition.Start, true);
        }

        private async void LihatJobButton_Clicked(object sender, EventArgs e)
        {
            await scrollView.ScrollToAsync(LihatJobSection, ScrollToPosition.Start, true);
        }
    }

    public class ImageItem
    {
        public string ImageSource { get; set; }
        public string Label1 { get; set; }
        public string Label2 { get; set; }
    }

    public class BulletPointItem
    {
        public string Text { get; set; }
    }
}
